<?php
namespace common\workers;

use common\models\ProfilePhoto;
use Imagick;
use Yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class ProfilePhotoCropJob extends BaseObject implements JobInterface
{
    /** @var int */
    public $photoIdOrigin;

    /** @var ProfilePhoto */
    protected $profilePhotoOrigin;

    /** @var Imagick */
    protected $imgOrigin;
    protected $imgGeoOrigin;

    public function execute($queue)
    {
        Yii::$app->log->getLogger()->log('ProfilePhotoCropJob_anegve', 1);
        $this->profilePhotoOrigin = ProfilePhoto::findOne(['id' => $this->photoIdOrigin]);

        try{
            $this->imgOrigin = new Imagick($this->profilePhotoOrigin->getPath());
        } catch (\ImagickException $e){
            var_dump($e);
        } catch (\Exception $e) {
            var_dump($e);
        }
        $this->imgGeoOrigin = $this->imgOrigin->getImageGeometry();
        $this->imgOrigin->setImageBackgroundColor('#fff');

        foreach (ProfilePhoto::getTypesInfo() as $type => $typeInfo) {
            $this->createByType($type, $typeInfo);
        }

        $this->imgOrigin->destroy();
        $this->profilePhotoOrigin->delete();
    }

    protected function createByType($type, $typeInfo)
    {
        $photo = new ProfilePhoto();
        $photo->profile_id = $this->profilePhotoOrigin->profile_id;
        $photo->ext = $this->profilePhotoOrigin->ext;
        $photo->type = $type;

        $img = $this->imgOrigin->clone();

        // width, height
        if (key_exists('width', $typeInfo) || key_exists('height', $typeInfo)) {

            $this->resize($img, $typeInfo['width'] ?? 0, $typeInfo['height'] ?? 0);

            if (key_exists('width', $typeInfo) && key_exists('height', $typeInfo)) {
                $img->setImageBackgroundColor(preg_match('/\.gif$|\.png$/', $this->profilePhotoOrigin->ext) == 1 ? 'None' : 'white');
                $imgGeo = $img->getImageGeometry();
                $img->extentImage(
                    $typeInfo['width'],
                    $typeInfo['height'],
                    ($imgGeo['width']-$typeInfo['width'])/2,
                    ($imgGeo['height']-$typeInfo['height'])/2
                );
            }
        }

        // MAX of (width, height)
        elseif (
            (key_exists('maxWidth', $typeInfo) || key_exists('maxHeight', $typeInfo))
            && ($this->imgGeoOrigin['width'] > $typeInfo['maxWidth'] || $this->imgGeoOrigin['height'] > $typeInfo['maxHeight'])
        ) {
            $this->resize($img, $typeInfo['maxWidth'] ?? 0, $typeInfo['maxHeight'] ?? 0);
        }

        // write to file
        if ($img->writeImage($photo->getPath())) {
            $photo->save();
        }

        $img->destroy();
    }

    /**
     * @param Imagick $img
     * @param int $width
     * @param int $height
     */
    protected function resize(&$img, $width, $height)
    {
        if ($this->imgGeoOrigin['width'] >= $this->imgGeoOrigin['height']) {
            $img->adaptiveResizeImage($width, 0);
        } else {
            $img->adaptiveResizeImage(0, $height);
        }

    }
}
