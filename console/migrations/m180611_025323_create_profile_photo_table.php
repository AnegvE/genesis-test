<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profile_photo`.
 */
class m180611_025323_create_profile_photo_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('profile_photo', [
            'id' => $this->primaryKey()->unsigned(),
            'profile_id' => $this->integer(11)->notNull()->unsigned(),
            'type' => $this->tinyInteger(1)->notNull(),
            'ext' => $this->string(10)->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('profile_photo');
    }
}
