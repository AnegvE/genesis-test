<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'console');
Yii::setAlias('@api', dirname(dirname(__DIR__)) . DIRECTORY_SEPARATOR . 'api');
Yii::setAlias('@uploads_public', DIRECTORY_SEPARATOR . 'uploads');
Yii::setAlias('@uploads', Yii::getAlias('@frontend') . DIRECTORY_SEPARATOR . 'web' . Yii::getAlias('@uploads_public'));

