<?php
namespace api\controllers;

use api\models\LoginForm;
use Yii;
use yii\rest\Controller;

class AuthController extends Controller
{
    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $tokenString = (string)$model->getToken();
            return [
                'success' => true,
                'token' => $tokenString,
            ];
        } else {
            return [
                'success' => false,
                'errors' => $model->getErrors(),
            ];
        }
    }
}
