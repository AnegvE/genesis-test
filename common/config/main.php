<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'queue' => [
            'class' => \yii\queue\amqp_interop\Queue::class,
            'serializer' => \yii\queue\serializers\JsonSerializer::class,
            'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            'attempts' => 1,
            'ttr' => 300,
        ],
    ],
    'bootstrap' => ['queue'],
];
