<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profile`.
 */
class m180611_025313_create_profile_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('profile', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(255)->notNull(),
            'email' => $this->string(255)->notNull(),
            'age' => $this->tinyInteger(3)->notNull()->unsigned(),
            'sex' => $this->tinyInteger(1)->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('profile');
    }
}
