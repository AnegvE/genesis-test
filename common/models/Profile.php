<?php

namespace common\models;

/**
 * This is the model class for table "profile".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $age
 * @property int $sex
 */
class Profile extends \yii\db\ActiveRecord
{
    const SEX_MALE = 1;
    const SEX_WOMAN = 2;

    public static function tableName()
    {
        return 'profile';
    }

    public function rules()
    {
        return [
            [['name', 'email', 'age', 'sex'], 'required'],
            [['name', 'email'], 'string', 'max' => 255],
            ['email', 'email'],
            ['age', 'integer', 'min' => 0, 'max' => 200],
            ['sex', 'integer'],
            ['sex', 'in', 'range' => [static::SEX_MALE, static::SEX_WOMAN]],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'age' => 'Age',
            'sex' => 'Sex',
        ];
    }

    /**
     * @return ProfilePhoto|null
     */
    public function getPhoto()
    {
        return ProfilePhoto::findOne(['profile_id' => $this->id, 'type' => ProfilePhoto::TYPE_NORMAL]);
    }

    public function beforeDelete()
    {
        foreach (ProfilePhoto::findAll(['profile_id' => $this->id]) as $photo) {
            $photo->delete();
        }

        return parent::beforeDelete();
    }


}
