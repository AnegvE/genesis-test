<?php
namespace api\models;

use common\models\Profile;
use Yii;
use yii\rest\ViewAction;

class ProfileViewAction extends ViewAction
{
    public function run($id)
    {
        /** @var Profile $model */
        $model = $this->findModel($id);
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        return [
            'id' => $model->id,
            'name' => $model->name,
            'email' => $model->email,
            'age' => $model->age,
            'sex' => $model->sex,
            'photo' => $model->getPhoto()->getPublicPath(),
        ];
    }
}
