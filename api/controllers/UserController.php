<?php

namespace api\controllers;

use api\models\ProfileForm;
use common\models\Profile;
use sizeg\jwt\JwtHttpBearerAuth;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public $modelClass = 'common\models\Profile';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => JwtHttpBearerAuth::class,
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        $actions['create']['modelClass'] = 'api\models\ProfileForm';
        $actions['update']['modelClass'] = 'api\models\ProfileForm';
        $actions['update']['findModel'] = function ($id, $action){
            $profile = Profile::findOne($id);
            $model = new ProfileForm([
                'scenario' => $action->scenario,
            ]);
            $model->id = $id;
            $model->load($profile->getAttributes());
            return $model;
        };
        $actions['view']['class'] = 'api\models\ProfileViewAction';
        return $actions;
    }


}
