<?php

namespace api\models;

use common\models\Profile;
use common\models\ProfilePhoto;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Login form
 *
 * @property string $name
 * @property string $email
 * @property int $age
 * @property int $sex
 * @property UploadedFile $photo
 */
class ProfileForm extends Model
{
    public $id;
    public $name;
    public $email;
    public $age;
    public $sex;
    public $photo;

    public function rules()
    {
        $rules = [
            [['name', 'email', 'age', 'sex'], 'trim'],
            [['name', 'email', 'age', 'sex'], 'required'],

            [['name', 'email'], 'string', 'max' => 255],

            ['email', 'email'],

            ['age', 'integer', 'min' => 0, 'max' => 200],

            ['sex', 'integer'],
            ['sex', 'in', 'range' => [Profile::SEX_MALE, Profile::SEX_WOMAN]],

            'photo' => ['photo', 'image', 'skipOnEmpty' => false, 'extensions' => 'jpg, gif, png, jpeg'],
        ];

        if ( !empty($this->id) && empty($_FILES['photo'])) {
            unset($rules['photo']);
        }

        return $rules;
    }


    public function formName()
    {
        return '';
    }

    public function load($data, $formName = null)
    {
        $result = parent::load($data, $formName);

        if ($result && !empty($_FILES['photo'])) {
            $this->photo = UploadedFile::getInstance($this, 'photo');
        }

        return $result;
    }


    public function save()
    {
        if ( !$this->validate()) {
            return false;
        }

        $profile = empty($this->id) ? (new Profile()) : Profile::findOne(['id' => $this->id]);
        $profile->name = $this->name;
        $profile->email = $this->email;
        $profile->age = $this->age;
        $profile->sex = $this->sex;

        if ( !$profile->save()) {
            return false;
        }

        if ( !empty($this->photo)) {
            $photo = ProfilePhoto::create($this->photo, $profile->id);
            $this->photo = $photo->getPublicPath();
        } else {
            $this->photo = $profile->getPhoto()->getPublicPath();
        }

        return true;
    }

    public function getPrimaryKey()
    {
        return Profile::primaryKey();
    }

    public function primaryKey()
    {
        return Profile::primaryKey();
    }
}
