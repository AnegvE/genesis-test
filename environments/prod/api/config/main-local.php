<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'jwt' => [
            // !!! insert a secret key in the following (if it is empty)
            'key' => '',
        ],
    ],
];