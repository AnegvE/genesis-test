<?php

namespace common\models;

use common\workers\ProfilePhotoCropJob;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "profile_photo".
 *
 * @property int $id
 * @property int $profile_id
 * @property int $type
 * @property string $ext
 *
 * @property Profile $profile
 */
class ProfilePhoto extends \yii\db\ActiveRecord
{
    const TYPE_RAW = 1;
    const TYPE_NORMAL = 2;
    const TYPE_PREVIEW = 3;

    public static $dirName = 'profile';

    public static function tableName()
    {
        return 'profile_photo';
    }

    public function rules()
    {
        return [
            [['profile_id', 'type', 'ext'], 'required'],
            [['profile_id', 'type'], 'integer'],
            [['ext'], 'string', 'max' => 10],
            ['type', 'in', 'range' => [static::TYPE_RAW, static::TYPE_NORMAL, static::TYPE_PREVIEW]],
            [
                ['profile_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Profile::className(),
                'targetAttribute' => ['profile_id' => 'id'],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'type' => 'Type',
            'ext' => 'Ext',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }

    public static function getTypesInfo()
    {
        return [
            static::TYPE_NORMAL => [
                'maxWidth' => 900,
                'maxHeight' => 900,
            ],
            static::TYPE_PREVIEW => [
                'width' => 100,
                'height' => 100,
            ],
        ];
    }

    /**
     * @param int $profileId
     * @param string $extension
     * @param int $type
     * @return string
     */
    public static function getPathStatic($profileId, $extension, $type)
    {
        return Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . static::$dirName . DIRECTORY_SEPARATOR . $profileId . '_' . $type . '.' . $extension;
    }

    public function getPath()
    {
        return static::getPathStatic($this->profile_id, $this->ext, $this->type);
    }

    public function getPublicPath()
    {
        return Yii::getAlias('@uploads_public') . DIRECTORY_SEPARATOR . static::$dirName . DIRECTORY_SEPARATOR . $this->profile_id . '_' . $this->type . '.' . $this->ext;
    }

    /**
     * @param UploadedFile $photo
     * @param int $profileId
     *
     * @return static
     */
    public static function create($photo, $profileId)
    {
        if ($photo->saveAs(static::getPathStatic($profileId, $photo->extension, static::TYPE_RAW))) {
            $model = new ProfilePhoto();
            $model->type = ProfilePhoto::TYPE_RAW;
            $model->profile_id = $profileId;
            $model->ext = $photo->extension;

            if ($model->save()) {
                Yii::$app->queue->push(new ProfilePhotoCropJob([
                    'photoIdOrigin' => $model->id,
                ]));
//                $job = new ProfilePhotoCropJob();
//                $job->photoIdOrigin = $model->id;
//                $job->execute(false);
                return $model;
            }
        }

        return null;
    }

    public function beforeDelete()
    {
        unlink($this->getPath());

        return parent::beforeDelete();
    }
}
